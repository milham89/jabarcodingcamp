void main(List<String> args) {
//   Segitiga1 segi1;
//   double luasSegi1;

//   segi1 = new Segitiga1();
//   segi1.setengah1 = 0.5;
//   segi1.alas1 = 20.0;
//   segi1.tinggi1 = 30.0;
//   luasSegi1 = segi1.hitungLuas1();
//   print(luasSegi1);
// }

// class Segitiga1 {
//   double? setengah1;
//   double? alas1;
//   double? tinggi1;

//   double hitungLuas1() {
//     return this.setengah1! * alas1! * tinggi1!;
//   }

  Segitiga segi;
  double luasSegi;

  segi = new Segitiga();
  segi.setSetengah(0.5);
  segi.setAlas(20.0);
  segi.setTinggi(30.0);

  luasSegi = segi.hitungLuas();
  print(luasSegi);
}

class Segitiga {
  double? _setengah;
  double? _alas;
  double? _tinggi;
  void setSetengah(double value) {
    if (value < 0) {
      value *= -1;
    }
    _setengah = value;
  }

  double getSetengah() {
    return _setengah!;
  }

  void setAlas(double value) {
    if (value < 0) {
      value *= -1;
    }
    _alas = value;
  }

  void setTinggi(double value) {
    if (value < 0) {
      value *= -1;
    }
    _tinggi = value;
  }

  double getAlas() {
    return _alas!;
  }

  double hitungLuas() {
    return this._setengah! * _alas! * _tinggi!;
  }
}

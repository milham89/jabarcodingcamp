import 'package:jccappflutter/Tugas/Tugas-13/global_resources.dart';

class TapIcon extends StatelessWidget {
  final String imageUrl;
  final String assetName;
  final String profileUrl;
  const TapIcon({Key key, this.imageUrl, this.profileUrl, this.assetName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        var url = profileUrl;
        if (await canLaunch(url)) {
          await launch(url, forceSafariVC: false);
        } else {
          throw 'couldn\'t launch $url';
        }
      },
      child: SvgPicture.asset(
        assetName,
        height: 24,
        width: 24,
      ),
    );
  }
}

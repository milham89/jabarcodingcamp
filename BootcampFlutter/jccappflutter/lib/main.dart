import 'package:flutter/material.dart';
// import 'package:jccappflutter/Tugas/Tugas-12/Telegram.dart';
// import 'package:jccappflutter/Tugas/Tugas-13/global_resources.dart';
import 'package:jccappflutter/Tugas/Tugas-14/models/get_data.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: GetDataScreen(),
    );
  }
}

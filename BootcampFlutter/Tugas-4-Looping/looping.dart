void main(List<String> arguments) {
  print("No. 1 Looping While ");
  print("LOOPING PERTAMA");
  var flag = 2;
  while (flag <= 20) {
    print("$flag - I Love Coding");
    flag += 2;
  }

  flag -= 2;
  print("\nLOOPING KEDUA");
  while (flag > 0) {
    print("$flag - I will become a mobile developer");
    flag -= 2;
  }

  print("\nNo. 2 Looping menggunakan for");
  for (var i = 1; i <= 20; i++) {
    // kelipatan tiga dan ganjil
    if (i % 3 == 0 && i % 2 == 1) {
      print("$i - I Love Coding");
    }
    // ganjil
    else if (i % 2 == 1) {
      print("$i - Santai");
    }
    // genap
    else if (i % 2 == 0) {
      print("$i - Berkualitas");
    }
  }

  print("\nNo. 3 Membuat Persegi Panjang #");
  for (var i = 0; i < 4; i++) {
    var text = '';
    for (var j = 0; j < 8; j++) {
      text += '#';
    }
    print(text);
  }

  print("\nNo. 4 Membuat Tangga");
  for (var i = 0; i < 7; i++) {
    var text = '';
    for (var j = 0; j <= i; j++) {
      text += '#';
    }
    print(text);
  }
}

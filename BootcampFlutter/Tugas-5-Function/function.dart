void main(List<String> arguments) {
  // Tugas 1
  String teriak() {
    return 'Halo Sanbers!';
  }

  print('No. 1');
  print(teriak());

  // Tugas 2
  int kalikan(int num1, int num2) {
    return num1 * num2;
  }

  print('\nNo. 2');
  var num1 = 12;
  var num2 = 4;
  var hasilKali = kalikan(num1, num2);
  print(hasilKali);

  // Tugas 3
  print('\nNo. 3');
  var name = 'Agus';
  var age = 30;
  var address = 'Jln. Malioboro, Yogyakarta';
  var hobby = 'Gaming';
  var perkenalan = (name, age, address, hobby) {
    return 'Nama saya $name, umur saya $age tahun, alamat saya di $address, dan saya punya hobby yaitu $hobby!';
  };
  print(perkenalan(name, age, address, hobby));

  // Tugas 4
  print('\nNo. 4');
  int faktorial(int angka) {
    if (angka <= 0) {
      return 1;
    } else {
      var total = angka;
      for (var i = angka - 1; i >= 1; i--) {
        total *= i;
      }
      return total;
    }
  }

  print(faktorial(6));
}

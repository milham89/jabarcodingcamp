import 'dart:io';

void main(List<String> args) {
  print('Soal No 1 ( Membuat Kalimat )');

  var word = 'Dart';
  var second = 'Is';
  var third = 'Awesome';
  var fourth = 'And';
  var fifth = 'I';
  var sixth = 'Love';
  var seventh = 'It!';
  print(word +
      " " +
      second +
      " " +
      third +
      " " +
      fourth +
      " " +
      fifth +
      " " +
      sixth +
      " " +
      seventh +
      " ");

  print('Soal No 2 ( Mengurai kalimat (Akses karakter dalam string)');

  var sentence = "I Am Going To Be Flutter Developer";
  var exampleFirstWord = sentence[0];
  var exampleSecondWord = sentence[2] + sentence[3];
  var thirdWord = sentence[4] +
      sentence[5] +
      sentence[6] +
      sentence[7] +
      sentence[8] +
      sentence[9];
  var fourthWord = sentence[11] + sentence[12];
  var fifthWord = sentence[14] + sentence[15];
  var sixthWord = sentence[16] +
      sentence[17] +
      sentence[18] +
      sentence[19] +
      sentence[20] +
      sentence[21] +
      sentence[21] +
      sentence[22] +
      sentence[23];
  var seventhWord = sentence[24] +
      sentence[25] +
      sentence[26] +
      sentence[27] +
      sentence[28] +
      sentence[29] +
      sentence[30] +
      sentence[31] +
      sentence[32] +
      sentence[33];

  print('First Word: ' + exampleFirstWord);
  print('Second Word: ' + exampleSecondWord);
  print('Third Word: ' + thirdWord);
  print('Fourth Word: ' + fourthWord);
  print('Fifth Word: ' + fifthWord);
  print('Sixth Word: ' + sixthWord);
  print('Seventh Word: ' + seventhWord);

  print(
      'Soal No 3 ( buatlah input dinamis yang akan menginput nama depan dan nama belakang dan jika di enter akan menggabungkan nama depan dan belakang )');

  print('Masukan Nama Depan: ');
  String inputText = stdin.readLineSync()!;
  print(" Masukan Nama Belakang: ");
  String inputText1 = stdin.readLineSync()!;
  print(inputText + " " + inputText1);

  print(
      'Soal No 4 ( menggunakan operator operasikan variable berikut ini menjadi bentuk operasi perkalian, penjumlahan, pengurangan dan pembagian )');

  var a = 5;
  var b = 10;
  var perkalian = a * b;
  var pembagian = a / b;
  var penambahan = a + b;
  var pengurangan = a - b;

  print(
      "Perkalian : $perkalian\nPembagian: $pembagian\nPenambahan: $penambahan\nPengurangan: $pengurangan ");
}

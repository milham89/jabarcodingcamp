Future<String> createOrderMessage() async {
  var order = await fetchUserOrder();

  return 'Get Data [done]  \n$order';
}

Future<String> fetchUserOrder() => Future.delayed(
      Duration(seconds: 3),
      () => 'Mikasa',
    );

Future<void> main() async {
  print('Luffy');
  print('Zorro');
  print('Killer');
  print('Nama Character One Piece :');
  print(await createOrderMessage());
}

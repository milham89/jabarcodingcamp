Future delayedPrint(int seconds, String message) {
  final duration = Duration(seconds: seconds);
  return Future.delayed(duration).then((value) => message);
}

main(List<String> args) {
  print('Live');
  delayedPrint(2, "Never Flat").then((status) {
    print(status);
  });
  print("Is");
}

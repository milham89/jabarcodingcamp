import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  Terjang t = Terjang();
  Punch p = Punch();
  Lempar l = Lempar();
  KillAlltitan k = KillAlltitan();

  t.powerPoint = 1;
  p.powerPoint = 2;
  l.powerPoint = 3;
  k.powerPoint = 4;

  print("Power Point terjang: ${t.powerPoint}");
  print("Power Point punch: ${p.powerPoint}");
  print("Power Point lempar: ${l.powerPoint}");
  print("Power Point killAlltitan: ${k.powerPoint}");
}

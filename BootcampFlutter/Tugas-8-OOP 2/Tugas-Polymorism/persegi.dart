import 'bangun_datar.dart';

class Persegi extends BangunDatar {
  num? s;

  Persegi({this.s});

  @override
  num luas() {
    return s! * s!;
  }

  @override
  num keliling() {
    return 4 * s!;
  }
}

import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main() {
  BangunDatar bangunDatar = BangunDatar();
  Persegi persegi = Persegi(s: 3);
  Segitiga segitiga = Segitiga(a: 3, t: 2, b: 7);
  Lingkaran lingkaran = Lingkaran(r: 7);

  bangunDatar.luas();
  print('Luas Persegi ${persegi.luas()}');
  print('Luas Segitiga ${segitiga.luas()}');
  print('Luas Lingkaran ${lingkaran.luas()}');

  bangunDatar.keliling();
  print('Keliling Persegi ${persegi.keliling()}');
  print('Keliling Segitiga ${segitiga.keliling()}');
  print('Keliling Lingkaran ${lingkaran.keliling()}');
}

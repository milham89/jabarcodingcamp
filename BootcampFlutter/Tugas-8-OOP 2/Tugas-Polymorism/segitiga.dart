import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  num? t;
  num? a;
  num? b;

  Segitiga({this.a, this.t, this.b});

  @override
  num luas() {
    return 1 / 2 * a! * t!;
  }

  @override
  num keliling() {
    return a! + b! + t!;
  }
}

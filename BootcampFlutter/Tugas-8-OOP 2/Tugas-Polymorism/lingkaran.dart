import 'bangun_datar.dart';

class Lingkaran extends BangunDatar {
  num? r;

  Lingkaran({this.r});

  @override
  num luas() {
    return 3.14 * r! * r!;
  }

  @override
  num keliling() {
    return 2 * 3.14 * r!;
  }
}

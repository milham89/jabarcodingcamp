import 'dart:io';

void main(List<String> args) {
  // 1. Ternary operator

  stdout.write('Akan Memilih Pilihan : ');
  String inputText = stdin.readLineSync()!;
  var opsi = inputText;

  (opsi == "y" || opsi != "t")
      ? print('Anda Akan Menginstall Aplikasi Dart')
      : print('aborted');
  print(opsi);

  // 2. If-else if dan else

  stdout.write('Masukkan Username Game : ');
  var nama = stdin.readLineSync();
  stdout.write('Masukan Peran Game : ');
  var peran = stdin.readLineSync();

  if (nama == "John" && peran == "") {
    print('Pilih Peranmu untuk memulai Game!');
  } else if (nama == "Jane" && peran == "Penyihir") {
    print(
        'Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!');
  } else if (nama == "Jenita" && peran == "Guard") {
    print(
        'Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.');
  } else if (nama == "Junaedi" && peran == "Werewolf") {
    print('Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!');
  } else {
    print('Harus Diisi Username Game dan Peran');
  }

  // 3. Switch case

  print(
      '1. Senin \n2. Selasa \n3. Rabu \n4. Kamis \n5. Jumat \n6. Sabtu \n7. Minggu ');
  stdout.write('Masukkan Input Hari : ');
  var namaHari = int.parse(stdin.readLineSync()!);

  switch (namaHari) {
    case 1:
      print(
          'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
      break;
    case 2:
      print(
          'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
      break;
    case 3:
      print(
          'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
      break;
    case 4:
      print(
          'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
      break;
    case 5:
      print('Hidup tak selamanya tentang pacar.');
      break;
    case 6:
      print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
      break;
    case 7:
      print(
          'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
      break;
    default:
  }

  // 4. Switch Case

  var hari = 10;
  var bulan = 2;
  var tahun = 2000;

  var err_hari = '';
  var err_bulan = '';
  var err_tahun = '';

  if (hari > 31 || hari <= 0) {
    err_hari = 'hari';
  }

  if (tahun >= 2200 || tahun <= 1900) {
    err_tahun = 'tahun';
  }

  if (bulan >= 13 || bulan < 1) {
    err_bulan = 'bulan';
  }

  if (err_tahun == '' && err_hari == '' && err_bulan == '') {
    switch (bulan) {
      case 1:
        print('$hari Januari $tahun');
        break;
      case 2:
        print('$hari Februari $tahun');
        break;
      case 3:
        print('$hari Maret $tahun');
        break;
      case 4:
        print('$hari April $tahun');
        break;
      case 5:
        print('$hari Mei $tahun');
        break;
      case 6:
        print('$hari Juni $tahun');
        break;
      case 7:
        print('$hari July $tahun');
        break;
      case 8:
        print('$hari Agustus $tahun');
        break;
      case 9:
        print('$hari September $tahun');
        break;
      case 10:
        print('$hari Oktober $tahun');
        break;
      case 11:
        print('$hari November $tahun');
        break;
      case 12:
        print('$hari Desember $tahun');
        break;
    }
  }

  if (err_tahun != '' || err_bulan != '' || err_hari != '') {
    var err_text = "";
    if (err_hari != '') {
      err_text = err_hari;
    }

    if (err_bulan != '') {
      err_text += ((err_text != "") ? ", " : '') + err_bulan;
    }

    if (err_tahun != '') {
      err_text += ((err_text != "") ? ", " : '') + err_tahun;
    }

    print("Error ${err_text}");
  }
}

void main(List<String> arguments) {
  print('Soal No. 1 (Range)');
  print(range(1, 10));
  print(range(11, 18));
  print(range(10, 1));

  print('\nSoal No. 2 (Range With Step)');
  print(rangeWithStep(1, 10, 2));
  print(rangeWithStep(11, 23, 3));
  print(rangeWithStep(5, 2, 1));

  print('\nSoal No. 3 (List Multidimensi)');
  List<dynamic> input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ];
  dataHandling(input);

  print('\nSoal No. 4 (Balik Kata)');
  print(balikKata("Kasur"));
  print(balikKata("SanberCode"));
  print(balikKata("Haji"));
  print(balikKata("racecar"));
  print(balikKata("Sanbers"));
}

// Soal No. 1 (Range)
List<int> range(int start, int finish) {
  List<int> result = [];
  if (start < finish) {
    for (int i = start; i <= finish; i++) {
      result.add(i);
    }
  } else if (start > finish) {
    for (int i = start; i >= finish; i--) {
      result.add(i);
    }
  }
  return result;
}

// Soal No. 2 (Range With Step)
List<int> rangeWithStep(int start, int finish, int step) {
  List<int> result = [];
  int counter = 1;

  if (start < finish) {
    for (int i = start; i <= finish; i++) {
      if (counter == 1) {
        result.add(i);
      }

      if (counter == step) {
        counter = 1;
      } else {
        counter++;
      }
    }
  } else if (start > finish) {
    for (int i = start; i >= finish; i--) {
      if (counter == 1) {
        result.add(i);
      }

      if (counter == step) {
        counter = 1;
      } else {
        counter++;
      }
    }
  }
  return result;
}

// Soal No. 4 (Balik Kata)
void dataHandling(List peoples) {
  peoples.forEach((people) {
    print('Nomor ID:  ${people[0]}');
    print('Nama Lengkap:  ${people[1]}');
    print('TTL:  ${people[2]} ${people[3]}');
    print('Hobi:  ${people[4]}');
    print('\n');
  });
}

// Soal No. 4 (Balik Kata)
String balikKata(String input) {
  return input.split('').reversed.join();
}
